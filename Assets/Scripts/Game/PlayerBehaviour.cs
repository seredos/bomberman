﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Game
{
    [RequireComponent(typeof(CharacterController))]
    public class PlayerBehaviour : MonoBehaviour
    {
        public float speed = 0.1f;
//        private Rigidbody _rigidbody;
        private CharacterController _characterController;
        private Vector2 _inputVector;

        private void Start()
        {
//            _rigidbody = GetComponent<Rigidbody>();
            _characterController = GetComponent<CharacterController>();
        }

        private void FixedUpdate()
        {
            _characterController.Move(new Vector3(_inputVector.x, 0, _inputVector.y) * speed);
        }


        public void Interact(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                Debug.Log("interact");
            }
        }

        public void Movement(InputAction.CallbackContext context)
        {
            _inputVector = context.ReadValue<Vector2>();
            //float speed = 0.1f;
            //_rigidbody.AddForce(new Vector3(inputVector.x, 0, inputVector.y) * speed, ForceMode.Force);
            //_characterController.Move(new Vector3(inputVector.x, 0, inputVector.y) * speed);
            //Debug.Log(context);
        }
    }
}